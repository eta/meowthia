#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>

int main() {
	char *cmd = getenv("IRCCAT_COMMAND");
	if (cmd == NULL || strcmp(cmd, "meowthia") != 0) {
		return 1;
	}
	char *argv[] = {"./inebriated", "-d", "/etc/meowthia.mkdb", "genonce", NULL};
	execvp("./inebriated", argv);
}
